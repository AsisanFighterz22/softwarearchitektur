package gui;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import logic.IPromoCodeLogic;
import java.awt.Color;

public class PromocodeGUI extends JFrame {

	private static final long serialVersionUID = 2303911575322895620L;
	private JPanel contentPane;
	private JLabel lblPromocode;

	/**
	 * Create the frame.
	 */
	public PromocodeGUI(IPromoCodeLogic logic) {
		setTitle("PromotionCodeGenerator");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 200);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(0, 153, 51));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		lblPromocode = new JLabel("");
		lblPromocode.setHorizontalAlignment(SwingConstants.CENTER);
		lblPromocode.setFont(new Font("Comic Sans MS", Font.BOLD, 18));
		contentPane.add(lblPromocode, BorderLayout.CENTER);
		
		JLabel lblBeschriftungPromocode = new JLabel("Mit den Promocodes k\u00F6nnen sie doppelte IT$ verdienen");
		lblBeschriftungPromocode.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblBeschriftungPromocode, BorderLayout.NORTH);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(0, 153, 0));
		contentPane.add(panel, BorderLayout.SOUTH);
		
		JButton btnCreatePromocode = new JButton("create Promocode");
		btnCreatePromocode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String code = logic.getNewPromoCode();
				lblPromocode.setText(code);
			}
		});
		btnCreatePromocode.setBackground(Color.LIGHT_GRAY);
		panel.add(btnCreatePromocode);
		
		
		JButton btnSavePromocodes = new JButton("save Promocodes");
		btnSavePromocodes.setBackground(Color.LIGHT_GRAY);
		panel.add(btnSavePromocodes);
		
		
		JButton btnExit = new JButton("exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			System.exit(0);
			}
		});
		btnExit.setBackground(Color.LIGHT_GRAY);
		panel.add(btnExit);
		this.setVisible(true);
	}

}
